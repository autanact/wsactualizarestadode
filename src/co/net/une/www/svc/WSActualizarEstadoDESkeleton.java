
/**
 * WSActualizarEstadoDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSActualizarEstadoDESkeleton java skeleton for the axisService
     */
    public class WSActualizarEstadoDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSActualizarEstadoDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param estadoNotificacion
         */
        

                 public co.net.une.www.gis.GisRespuestaGeneralType_GDE actualizarEstadoDE
                  (
                  java.lang.String codigoSolicitud,java.lang.String estadoNotificacion
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("estadoNotificacion",estadoNotificacion);
		try{
		
			return (co.net.une.www.gis.GisRespuestaGeneralType_GDE)
			this.makeStructuredRequest(serviceName, "actualizarEstadoDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    